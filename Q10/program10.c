#include<stdio.h>
#include<math.h>

float marks1, marks2, marks3, marks4, marks5, sum, percentage;
int main() {
    take_marks();
    calc_percent();
    check_grade();
    return 0;
}

int take_marks() {
    printf("\nEnter marks of Subject 1 : ");
    scanf("%f", &marks1);
    printf("\nEnter marks of Subject 2 : ");
    scanf("%f", &marks2);
    printf("\nEnter marks of Subject 3 : ");
    scanf("%f", &marks3);
    printf("\nEnter marks of Subject 4 : ");
    scanf("%f", &marks4);
    printf("\nEnter marks of Subject 5 : ");
    scanf("%f", &marks5);
    return 0;
}

int calc_percent() {
    sum = marks1 + marks2 + marks3 + marks4 + marks5;
    percentage = (sum/500)*100;
}

int check_grade() {
    if (percentage >= 90 || percentage >= 100) {
        printf("\n\nYour grade is A and percentage is %.2f", percentage);
    }
    else if (percentage >= 80 || percentage < 90) {
        printf("\n\nYour grade is B and percentage is %.2f", percentage);
    }
    else if (percentage >= 60 || percentage < 80) {
        printf("\n\nYour grade is C and percentage is %.2f", percentage);
    }
    else if (percentage < 60) {
        printf("\n\nYour grade is D and percentage is %.2f", percentage);
    }
    else {
        printf("\n\nYour grade is E and percentage is %.2f", percentage);
    }
}
