#include<stdio.h>
#include<math.h>

int main() {
    int num;
    printf("\nEnter the number : ");
    scanf("%d", &num);
    if (num%2 == 0) {
        printf("\nThe number entered is even");
    }
    else {
        printf("\nThe number entered is odd");
    }
    return 0;
}
