#include<stdio.h>

int main() {
    float x, y;
    printf("Enter first number : ");
    scanf("%f", &x);
    printf("\nEnter second number : ");
    scanf("%f", &y);
    if (x == y) {
        printf("\nThe two numbers entered are equal.");
    }
    else {
        printf("\nThe two numbers entered are not equal.");
    }
    return 0;
}
