#include<stdio.h>
#include<math.h>

int main() {
    float ctemp, ftemp;
    printf("\nEnter temperature (in centigrade) : ");
    scanf("%f", &ctemp);
    ftemp = (ctemp*9/5) + 32;
    printf("\nTemperature in fahrenheit is %.1f", ftemp);
    return 0;
}
