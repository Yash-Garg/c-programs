#include<stdio.h>
#include<math.h>

int main() {
    int n;
    long fact = 1;
    printf("Enter the number you want to calculate factorial for : ");
    scanf("%d", &n);
    for(int i=1; i<=n; i++) {
        fact = fact * i;
    }
    printf("\nFactorial of the number %d is %d", n, fact);
    return 0;
}
