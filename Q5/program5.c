#include<stdio.h>

int main() {
    int x, y, z;
    printf("\nEnter two integers : ");
    scanf("%d%d", &x, &y);
    printf("\nBefore Swapping \nFirst integer = %d \nSecond integer = %d", x, y);
    z = x;
    x = y;
    y = z;
    printf("\n\nAfter Swapping \nFirst integer = %d \nSecond integer = %d", x, y);
    return 0;
}
