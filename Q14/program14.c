#include<stdio.h>
#include<math.h>

int main () {
	int i, n, oddsum = 0, evensum = 0;
	printf("Enter the number till u want the sum : ");
	scanf("%d", &n);
	for(i=1; i<=n; i++) {
		if(i%2 == 0) {
			evensum = evensum + i;
		} else {
			oddsum = oddsum + i;
		}
	}
	printf("\nSum of even numbers upto %d is : %d", n, evensum);
	printf("\nSum of odd numbers upto %d is : %d", n, oddsum);
	return 0;
}
