#include<stdio.h>
#include<math.h>

float p, r, t, si, ci, amt;
int x;
int main() {
    printf("WELCOME TO INTEREST CALCULATOR !");
    printf("\n\nEnter the Principal Amount (in INR): ");
    scanf("%f", &p);
    printf("Enter the Rate of Interest : ");
    scanf("%f", &r);
    printf("Enter the Time Period (in Years) : ");
    scanf("%f", &t);
    printf("\nWhat would you like to calculate? \n1. Simple Interest \n2. Compound Interest \n3. Both");
    printf("\nEnter your choice (1-3) : ");
    scanf("%d", &x);

    if (x == 1) {
        si_calc();
    }
    else if (x == 2) {
        ci_calc();
    }
    else if (x == 3) {
        si_calc();
        ci_calc();
    }
    else {
        exit(0);
    }

    return 0;
}

int si_calc() {
    si = (p*r*t)/100;
    printf("\n\nSimple Interest : %.2f", si);
    return 0;
}

int ci_calc() {
    amt = p*(pow((1+r/100), t));
    ci = amt - p;
    printf("\nCompound Interest : %.2f", ci);
    return 0;
}
