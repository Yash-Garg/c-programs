#include<stdio.h>
#include<math.h>

int main() {
    int a;
    float x, y, result;
    printf("Enter first number : ");
    scanf("%f", &x);
    printf("Enter second number : ");
    scanf("%f", &y);
    printf("\n\n------M E N U------\n 1. Addition (+)\n 2. Subtraction (-)\n 3. Multiplication (*)\n 4. Division (/)\n\n");
    printf("What operation do u want to perform between the two numbers? (1-4) : ");
    scanf("%d", &a);
    switch(a) {
        case 1: result = x + y;
                printf("\nAddition of the numbers is %.2f", result);
                break;
        case 2: result = x - y;
                printf("\nSubtraction of the numbers is %.2f", result);
                break;
        case 3: result = x * y;
                printf("\nMultiplication of the numbers is %.2f", result);
                break;
        case 4: result = x / y;
                printf("\nDivision of the numbers is %.2f", result);
                break;
    }
}
