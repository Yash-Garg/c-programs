#include<stdio.h>
#include<math.h>

int year;
int main() {
    printf("\nEnter the year you want to check leap year for : ");
    scanf("%d", &year);
    check_leap_year();
    return 0;
}

int check_leap_year() {
    if (year%4 == 0) {
        if (year%100 == 0) {
            if (year%400 == 0)
                printf("\nThe year %d is a leap year", year);
            else
                printf("\nThe year %d is not a leap year", year);
        } else
            printf("\nThe year %d is a leap year", year);
    } else
        printf("\nThe year %d is not a leap year", year);
}
