#include<stdio.h>
#include<math.h>

float marks1, marks2, marks3, marks4, marks5, sum=0, percentage=0;
int main() {
    take_input();
    add();
    calc_percent();
    output();
    return 0;
}

int take_input() {
    printf("Maximum Marks are 100, give input accordingly!\n");
    printf("\nEnter marks of Subject 1 : ");
    scanf("%f", &marks1);
    printf("\nEnter marks of Subject 2 : ");
    scanf("%f", &marks2);
    printf("\nEnter marks of Subject 3 : ");
    scanf("%f", &marks3);
    printf("\nEnter marks of Subject 4 : ");
    scanf("%f", &marks4);
    printf("\nEnter marks of Subject 5 : ");
    scanf("%f", &marks5);
    return 0;
}

int add() {
    sum = marks1 + marks2 + marks3 + marks4 + marks5;
    return 0;
}

int calc_percent() {
    percentage = (sum/500)*100;
    return 0;
}

int output() {
    printf("\n\nThe total marks obtained are %.1f", sum);
    printf("\nThe percentage of marks obtained is %.1f percent", percentage);
    return 0;
}
