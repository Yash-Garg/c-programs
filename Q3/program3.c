#include<stdio.h>
#include<math.h>

#define pi 3.14

float area, circumference, radius;

int main() {
    printf("\nEnter the radius of circle : ");
    scanf("%f", &radius);
    area = (pow(pi, 2))*radius;
    circumference = 2*pi*radius;
    printf("\nArea of the circle is %.3f", area);
    printf("\nCircumference of the circle is %.3f", circumference);
    return 0;
}
